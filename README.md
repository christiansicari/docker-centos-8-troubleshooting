# Docker on Centos 8
Guida all'installazione e al troubleshooting

## Installazione
L'installazione nella maggior parte dei casi richiede la rimozione buildah e podman che creano conflitti con runc, richiesto invece da Docker.
Seguendo la guida di Docker.com dobbiamo
1. Aggiungere le repo
```
sudo yum install -y yum-utils;
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo;
```

2. Installare pacchetti docker e rimuovere podman e buildadh (usando --allowerasing)
```
sudo yum install docker-ce docker-ce-cli containerd.io ;
sudo yum install docker-ce docker-ce-cli containerd.io --nobest --allowerasing;
```
Testare funzionamento
```
docker run --rm hello-world
```

## Troubleshooting

### DNS resolv
Di default i container non sono abilitati ad usare le interfacce di rete risolvere le query DNS. Bisogna abilitare le interfacce dal firewall

```
firewall-cmd --permanent --zone=trusted --add-interface=docker0;
firewall-cmd --reload
```
####
Test funzionamento
```
 docker run  --rm bash ping -c 5
google.com
```

### Internal DNS
Anche con una user defined network i container non sono capaci di risolvere gli hostname dei container nella stessa rete, risolvere con
```
sudo firewall-cmd --permanent --zone=public --add-masquerade

firewall-cmd --reload
```
